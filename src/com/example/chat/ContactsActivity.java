package com.example.chat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.chat.libChat.Connection;
import com.example.chat.libChat.Contact;

import java.util.ArrayList;

/**
 * Created by araste on 01/07/2015.
 */
public class ContactsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_activity);

        ListView contactsListView = (ListView) findViewById(R.id.contacts_list);
        final ArrayList<Contact> contactArrayList = Connection.getInstance().getContactsList();
        ContactListViewAdapter contactListViewAdapter = new ContactListViewAdapter(this,contactArrayList);
        contactsListView.setAdapter(contactListViewAdapter);

        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ContactsActivity.this,ChatActivity.class);
                intent.putExtra("contact",contactArrayList.get(i).getUserName());
                startActivity(intent);
            }
        });




    }
}
