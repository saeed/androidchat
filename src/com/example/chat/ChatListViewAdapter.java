package com.example.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.chat.libChat.ChatMessage;
import org.jivesoftware.smack.Chat;

import java.util.ArrayList;

/**
 * Created by araste on 01/07/2015.
 */
public class ChatListViewAdapter extends ArrayAdapter<ChatMessage> {

    Context context;
    ArrayList<ChatMessage> contactArrayList;

    public ChatListViewAdapter(Context context, ArrayList<ChatMessage> chatItems){
        super(context,R.layout.contact_list_item,chatItems);

        this.context = context;
        this.contactArrayList = chatItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.chat_list_item,parent,false);

        TextView from = (TextView) rowView.findViewById(R.id.text_view_from);
        TextView body = (TextView) rowView.findViewById(R.id.text_view_body);
        TextView time = (TextView) rowView.findViewById(R.id.text_view_time);
        TextView packetId = (TextView) rowView.findViewById(R.id.text_view_packetId);
        TextView status = (TextView) rowView.findViewById(R.id.status_text_view);

        from.setText(contactArrayList.get(position).getSenderUserId());
        body.setText(contactArrayList.get(position).getBody());
        time.setText(contactArrayList.get(position).getTime().toString());
        packetId.setText(contactArrayList.get(position).getPacketId());
        status.setText(contactArrayList.get(position).getStatus());

        return rowView;
    }
}
