package com.example.chat.libChat;

import java.util.Date;

/**
 * Created by araste on 01/26/2015.
 */
public class ChatMessage {
    String senderUserId;
    String receiverUserId;
    Date time;
    String body;
    String status;
    String packetId;

    public ChatMessage(){}

    public ChatMessage (String senderUserId, String receiverUserId, Date time, String body, String status, String packetId){
        this.senderUserId = senderUserId;
        this.receiverUserId = receiverUserId;
        this.time = time;
        this.body = body;
        this.status = status;
        this.packetId = packetId;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public String getReceiverUserId() {
        return receiverUserId;
    }

    public String getBody() {
        return body;
    }

    public Date getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public String getPacketId() {
        return packetId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public void setReceiverUserId(String receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
