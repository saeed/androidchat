package com.example.chat.libChat;

import com.example.chat.libChat.ChatMessage;

/**
 * Created by araste on 01/13/2015.
 */
public interface OnMessageReceivedListener {

    void messageReceived(ChatMessage msg);

}
