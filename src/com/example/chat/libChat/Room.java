package com.example.chat.libChat;

import org.jivesoftware.smackx.packet.VCard;

/**
 * Created by araste on 01/07/2015.
 */
public class Room {

    private String roomName;
//    private String status;
//    private VCard vCard;
//    private String lastActivity;

    Room(String roomName){
        this.roomName = roomName;
    }

    public String getRoomName() {
        return roomName;
    }
}
