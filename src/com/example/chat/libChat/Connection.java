package com.example.chat.libChat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.Log;
import com.example.chat.libChat.Command.Command;
import com.example.chat.libChat.Command.NotificationCommand;
import com.example.chat.libChat.Command.PrivateChatCommand;
import com.example.chat.libChat.Command.RoomChatCommand;
import com.example.chat.libChat.database.LibChatDatabaseCommand;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.*;
import org.jivesoftware.smackx.bytestreams.ibb.packet.Data;
import org.jivesoftware.smackx.filetransfer.FileTransfer;
import org.jivesoftware.smackx.muc.*;
import org.jivesoftware.smackx.packet.DelayInformation;
import org.jivesoftware.smackx.packet.LastActivity;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.smackx.provider.DelayInformationProvider;
import org.jivesoftware.smackx.provider.MUCAdminProvider;
import org.jivesoftware.smackx.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.provider.MUCUserProvider;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by araste on 01/07/2015.
 */
public class Connection {

    private static Connection connectionInstance;
    public static final String HOST = "robotoos.ir";

    public static final int PORT = 5222;
    public static final String SERVICE = "robotoos";
    public static final String USERNAME = "sudomakeinstall2" + "@" + SERVICE;
    //    public static final String PASSWORD = "saeidsaeid";
    public static final String PASSWORD = "sudoaptgetpurge2";

//    private static final String COMMAND_TYPE_NOTIFICATION = "notification" ;
//    private static final String COMMAND_TYPE_PRIVATE_MESSAGE = "private_message" ;


    public static final String CONNECTION_STATUS_CONNECTED = "connected";
    public static final String CONNECTION_STATUS_CONNECTING = "connecting";
    public static final String CONNECTION_STATUS_DISCONNECTED = "disconnected";

    // command queue
    private JSONArray commandQueue ;

    // Listeners
    private ArrayList<OnMessageReceivedListener> onMessageReceivedListeners;
    private ArrayList<OnRoomMessageReceivedListener> onRoomMessageReceivedListeners;
    private ArrayList<OnMessageNotificationReceivedListener> onMessageNotificationReceivedListeners;
    private ArrayList<OnRoomMessageNotificationReceivedListener> onRoomMessageNotificationReceivedListeners;

    // Rooms
    private HashMap<String,MultiUserChat> roomsMap;

    //private Context context;
    private XMPPConnection connection;
    private MessageEventManager messageEventManager;

    private Connection() {
        onMessageReceivedListeners = new ArrayList<OnMessageReceivedListener>();
        onMessageNotificationReceivedListeners = new ArrayList<OnMessageNotificationReceivedListener>();
        onRoomMessageReceivedListeners = new ArrayList<OnRoomMessageReceivedListener>();
        onRoomMessageNotificationReceivedListeners = new ArrayList<OnRoomMessageNotificationReceivedListener>();
        roomsMap = new HashMap<String, MultiUserChat>();
//        initCommandQueue();
//        initRooms();
        initConnection();
        connect();
    }

    private void initCommandQueue() {
        DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
        try {
            commandQueue = databaseHandler.getAllCommands();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void initConnection(){
        // Create a connection
        ConnectionConfiguration connConfig = new ConnectionConfiguration(HOST, PORT, SERVICE);
//                connConfig.setReconnectionAllowed(true);
        connection = new XMPPConnection(connConfig);
//                SmackAndroid.init(context);
        ProviderManager.getInstance().addExtensionProvider("x", "jabber:x:delay", new DelayInformationProvider());
        ProviderManager.getInstance().addIQProvider("vCard ", "vcard-temp", new org.jivesoftware.smackx.provider.VCardProvider());
        // MUC User
        ProviderManager pm = ProviderManager.getInstance();
        pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
                new MUCUserProvider());
        // MUC Admin
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
                new MUCAdminProvider());
        // MUC Owner
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
                new MUCOwnerProvider());

        // connectionListener

    }

    private void initRooms() {
        List<Room> rooms = DatabaseHandler.getInstance().getAllRooms();
        for ( Room room : rooms){
            MultiUserChat muc = new MultiUserChat(connection, room.getRoomName());
            roomsMap.put(room.getRoomName(),muc);
            joinRoom(muc);
        }
    }

    private void joinRoom(MultiUserChat muc) {
        /// join room
        try {
//            DiscussionHistory discussionHistory = new DiscussionHistory();
//            discussionHistory.setMaxStanzas(5);
//            muc.join("testbot", null, discussionHistory, 10);
            muc.join(USERNAME);
        } catch (XMPPException e1) {
            e1.printStackTrace();
        }

        /// setup message listener
        muc.addMessageListener(new PacketListener() {
            @Override
            public void processPacket(Packet packet) {
                Message message = (Message) packet;
                onRoomMessageReceived(message);
                Log.d("libchat","multi chat msg received:"+message.getBody());
            }
        });

        /// invitation
        muc.addInvitationRejectionListener(new InvitationRejectionListener() {
            public void invitationDeclined(String invitee, String reason) {
                Log.d("libchat","this person:"+invitee+" rejected invtied because:"+reason);
            }
        });

        // subject change listener
        muc.addSubjectUpdatedListener(new SubjectUpdatedListener() {
            @Override
            public void subjectUpdated(String subject, String from) {
                Log.d("libchat", "subject changed to " + subject + " by this " + from);
            }
        });
    }

    private void setupNotificationListener() {

        messageEventManager.addMessageEventRequestListener(new DefaultMessageEventRequestListener() {
            public void deliveredNotificationRequested(
                    String from,
                    String packetID,
                    MessageEventManager messageEventManager) {
                super.deliveredNotificationRequested(from, packetID, messageEventManager);
// DefaultMessageEventRequestListener automatically responds that the message was delivered when receives this r
                System.out.println("Delivered Notification Requested (" + from + ", " + packetID + ")");
            }

            public void displayedNotificationRequested(String from, String packetID, MessageEventManager messageEventManager) {
                super.displayedNotificationRequested(from, packetID, messageEventManager);
// Send to the message's sender that the message was
                System.out.println("Displayed Notification Requested (" + from + ", " + packetID + ")");
//                        messageEventManager.sendDisplayedNotification(from, packetID);
            }

            public void composingNotificationRequested(String from, String packetID, MessageEventManager messageEventManager) {
                super.composingNotificationRequested(from, packetID, messageEventManager);
// Send to the message's sender that the message's receiver is composing a reply
                System.out.println("Composing Notification Requested (" + from + ", " + packetID + ")");
                //messageEventManager.sendComposingNotification(from, packetID);
            }

            public void offlineNotificationRequested(String from, String packetID, MessageEventManager messageEventManager) {
                super.offlineNotificationRequested(from, packetID, messageEventManager);
// The XMPP server should take care of this request. Do nothing.
                System.out.println("Offline Notification Requested (" + from + ", " + packetID + ")");
            }
        });

        messageEventManager.addMessageEventNotificationListener(new LibChatMessageEventNotificationListener() );
    }

    public void setNotificationRequestForMessage(Message message) {
        /// https://www.igniterealtime.org/builds/smack/docs/latest/documentation/extensions/messageevents.html
        MessageEventManager.addNotificationsRequests(message, true, true, true, true);
    }

    public static Connection getInstance() {
        if (connectionInstance == null) {
            connectionInstance = new Connection();
        }
        return connectionInstance;
    }

    public ArrayList<Contact> getContactsList(){
        if ( connection.isAuthenticated() )
            return getContactsListOnline();
//        ArrayList<Contact> contactArrayList = new ArrayList<Contact>();
        List<Contact> contactList = DatabaseHandler.getInstance().getAllContacts();
        return (ArrayList<Contact>) contactList;
    }

    public ArrayList<Contact> getContactsListOnline() {
        ArrayList<Contact> contactArrayList = new ArrayList<Contact>();

        Roster roster = connection.getRoster();
        Collection<RosterEntry> entries = roster.getEntries();
        for (RosterEntry entry : entries) {

            Log.d("XMPPChatDemoActivity", "--------------------------------------");
            Log.d("XMPPChatDemoActivity", "RosterEntry " + entry);
            Log.d("XMPPChatDemoActivity", "User: " + entry.getUser());
            Log.d("XMPPChatDemoActivity", "Name: " + entry.getName());
            Log.d("XMPPChatDemoActivity", "Status: " + entry.getStatus());
            Log.d("XMPPChatDemoActivity", "Type: " + entry.getType());
            Presence entryPresence = roster.getPresence(entry.getUser());

            Log.d("XMPPChatDemoActivity", "Presence Status: " + entryPresence.getStatus());
            Log.d("XMPPChatDemoActivity", "Presence Type: " + entryPresence.getType());

            Presence.Type type = entryPresence.getType();
            if (type == Presence.Type.available)
                Log.d("XMPPChatDemoActivity", "Presence AVIALABLE");
            Log.d("XMPPChatDemoActivity", "Presence : " + entryPresence);

            VCard vCard = new VCard();
            try {
                vCard.load(connection, entry.getUser());
            } catch (XMPPException e) {
                Log.d("libchat", "vcard load error");
                e.printStackTrace();
            }

            String activity = "";
            try {
                LastActivity lastActivity = LastActivity.getLastActivity(connection, entry.getUser());
                long seconds = lastActivity.lastActivity;
                int day = (int) TimeUnit.SECONDS.toDays(seconds);
                long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
                long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
                long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
                //activity = String.valueOf(lastActivity.lastActivity);
                activity = String.format("%d days %d hours %d minutes %d seconds", day, hours, minute, second);
            } catch (XMPPException e) {
                Log.d("libchat", "last activity error");
                e.printStackTrace();
            }

            Contact contact = new Contact(entry.getUser(), entryPresence.getStatus(), vCard, activity);
            contactArrayList.add(contact);
        }
        return contactArrayList;
    }

    public void addContact(String UserId) {
//        Presence response = new Presence.Type.subscribed);
//        response.setTo(address);
//        sendPacket(response);

        Roster roster = connection.getRoster();
        try {
            roster.createEntry(UserId, null, null);
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    public void connect() {

//        final ProgressDialog dialog = ProgressDialog.show(context, "Connecting...", "Please wait...", false);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                // connect
                while ( connection.isConnected()==false ) {
                    SmackConfiguration.setPacketReplyTimeout(10000);

                    Log.i("libchat", "attempt to connect " + connection.getHost());
                    try {
                        connection.connect();
                        connection.addConnectionListener(new ConnectionListener() {
                            @Override
                            public void connectionClosed() {
                                Log.d("libchat","connection got closed");
                            }

                            @Override
                            public void connectionClosedOnError(Exception e) {
                                Log.d("libchat","connection got closed on error");
                                e.printStackTrace();
                            }

                            @Override
                            public void reconnectingIn(int i) {
                                Log.d("libchat","reconnecting in "+String.valueOf(i));
                            }

                            @Override
                            public void reconnectionSuccessful() {
                                Log.d("libchat","reconnected successfully");
                            }

                            @Override
                            public void reconnectionFailed(Exception e) {
                                Log.d("libchat","reconnection failed");
                                e.printStackTrace();
                            }
                        });

                        Log.i("libchat", "connected to " + connection.getHost());
                        authenticate();

                        setupConnection();
//                        connection.login(USERNAME, PASSWORD);
//                        Log.i("libchat", "logged in " + connection.getHost());
//                        return;
                    } catch (Exception ex) {
                        Log.e("libchat", "Failed to connect to " + connection.getHost());
                        Log.e("libchat", ex.toString());
                        try {
                            Log.i("libchat", "try to connect after 5 second " + connection.getHost());
                            Thread.sleep(5);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        t.start();
//        dialog.show();
    }

    private void setupConnection() {
        Log.d("libchat","isConnected:"+connection.isConnected()+" isAuthenticated:"+connection.isAuthenticated());

        // Set the status to available
        Presence presence = new Presence(Presence.Type.available);
        connection.sendPacket(presence);




        // notifications
        messageEventManager = new MessageEventManager(connection);
        setupNotificationListener();

        // Add a packet listener to get messages sent to us
        PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
        connection.addPacketListener(new PacketListener() {
            @Override
            public void processPacket(Packet packet) {
                Message message = (Message) packet;
                onMessageReceived(message);
                // Add the incoming message to the list view
//                        mHandler.post(new Runnable() {
//                            public void run() {
//                                setListAdapter();
//                            }
//                        });
            }

        }, filter);

        // add a packet Lister to get presene subscriptions
        filter = new PacketTypeFilter(Presence.class);
        connection.addPacketListener(new PacketListener() {
            @Override
            public void processPacket(Packet packet) {
                Presence presence = (Presence) packet;
                if (presence.getType() == Presence.Type.subscribe) {
                    // someone wants to listen to your presence
                    Log.i("chat lib ", " presence subscribe ");
                    Presence acceptRequest = new Presence(Presence.Type.subscribed);
//                        acceptRequest.setMode(Presence.Mode.available);
//                        acceptRequest.setPriority(24);
                    acceptRequest.setTo(presence.getFrom());
                    connection.sendPacket(acceptRequest);

                } else if (presence.getType() == Presence.Type.available) {
                    Log.i("chat lib ", " presence availabe ");
                } else if (presence.getType() == Presence.Type.unavailable) {
                    Log.i("chat lib ", " presence unavailable ");
                } else if (presence.getType() == Presence.Type.subscribed) {
                    // someone accepted your subscribe request
                    Log.i("chat lib ", " presence subscribed ");
                } else if (presence.getType() == Presence.Type.error) {
                    Log.i("chat lib ", " presence error ");
                } else if (presence.getType() == Presence.Type.unsubscribed) {
                    Log.i("chat lib ", " presence unsubscribed ");
                } else if (presence.getType() == Presence.Type.unsubscribe) {
                    // user (presence.from) says you can listen to my presence
                    Log.i("chat lib ", " presence unsubscribe ");
                } else {
                    Log.i("chat lib ", " no such presence type ");
                }
            }
        }, filter);

        initRooms();



        Roster roster = connection.getRoster();
        roster.setSubscriptionMode(Roster.SubscriptionMode.manual);
        roster.addRosterListener(new RosterListener() {
            @Override
            public void entriesAdded(Collection<String> collection) {
                Log.i("libchat", "roster listener: add");
            }

            @Override
            public void entriesUpdated(Collection<String> collection) {
                Log.i("libchat", "roster listener: update");
            }

            @Override
            public void entriesDeleted(Collection<String> collection) {
                Log.i("libchat", "roster listener: delete");
            }

            @Override
            public void presenceChanged(Presence presence) {
                Log.i("libchat", "roster listener: change");
            }
        });

        runCommandQueue();

    }

    private void authenticate() {
        // login
        while ( connection.isAuthenticated() == false ) {
            Log.i("libchat","trying to login");
            try {
                connection.login(USERNAME, PASSWORD);
                Log.i("libchat", "Logged in as" + connection.getUser());
            } catch (XMPPException e) {
                Log.e("libchat", "failed to login");
                e.printStackTrace();

                try {
                    Log.i("libchat","retry for login in 5 sec");
                    Thread.sleep(5);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void sendRoomMessage(String room,String body,ChatMessage chatMessage){
        Message message = new Message(room, Message.Type.groupchat);
        message.setBody(body);
        if ( chatMessage == null )
            chatMessage = new ChatMessage();
        chatMessage.setSenderUserId(USERNAME);
        chatMessage.setReceiverUserId(room);
        chatMessage.setTime(new Date());
        chatMessage.setPacketId(message.getPacketID());
        chatMessage.setBody(body);
        boolean successful = false;
        if ( connection.isAuthenticated() ){
            MultiUserChat multiUserChat = roomsMap.get(room);
            try {
                setNotificationRequestForMessage(message);
                multiUserChat.sendMessage(message);
                chatMessage.setStatus(Constants.MESSAGE_STATUS_SENT);
                DatabaseHandler.getInstance().saveRoomMessageToDatabase(chatMessage);
                Log.d("libchat","successfully send message: "+message.getBody()+" to room "+room);
                successful = true;
            } catch (XMPPException e) {
                successful = false;
                e.printStackTrace();
            }
        }
        if ( successful == false ){
            chatMessage.setStatus(Constants.MESSAGE_STATUS_PENDING);
            DatabaseHandler.getInstance().saveRoomMessageToDatabase(chatMessage);
            addCommandToQueue(new RoomChatCommand(chatMessage));
        }
    }

    public void retrySendingRoomMessage(Message message, String roomName) throws XMPPException {
        MultiUserChat multiUserChat = roomsMap.get(roomName);
//        try {
            multiUserChat.sendMessage(message);
            Log.d("libchat","retry finally send message: "+message.getBody()+" to room "+roomName);
//        } catch (XMPPException e) {
//            e.printStackTrace();
//        }
    }

    public void sendMessage(String to,String body,ChatMessage chatMessage){
        Message message = new Message(to, Message.Type.chat);
        message.setBody(body);
        if ( chatMessage == null )
            chatMessage = new ChatMessage();
        chatMessage.setSenderUserId(USERNAME);
        chatMessage.setReceiverUserId(to);
        chatMessage.setTime(new Date());
        chatMessage.setPacketId(message.getPacketID());
        chatMessage.setBody(body);
        if ( connection.isAuthenticated() ) {
            setNotificationRequestForMessage(message);
            connection.sendPacket(message);
            chatMessage.setStatus(Constants.MESSAGE_STATUS_SENT);
            DatabaseHandler.getInstance().saveMessageToDatabase(chatMessage);
        }
        else{
            chatMessage.setStatus(Constants.MESSAGE_STATUS_PENDING);
            DatabaseHandler.getInstance().saveMessageToDatabase(chatMessage);
            addCommandToQueue(new PrivateChatCommand(chatMessage));
//            addCommandToQueue(COMMAND_TYPE_PRIVATE_MESSAGE, chatMessage.getReceiverUserId(), chatMessage.getBody());
        }
    }

    public void disconnect() {
        connection.disconnect();
    }

    public void addOnMessageReceivedListener(OnMessageReceivedListener l) {
        onMessageReceivedListeners.add(l);
    }

    public void addOnRoomMessageReceivedListener(OnRoomMessageReceivedListener l){
        onRoomMessageReceivedListeners.add(l);
    }

    public void addOnMessageNotificationReceivedListener(OnMessageNotificationReceivedListener l) {
        onMessageNotificationReceivedListeners.add(l);
    }

    public void addOnRoomMessageNotificationReceivedListener(OnRoomMessageNotificationReceivedListener l){
        onRoomMessageNotificationReceivedListeners.add(l);
    }

    private void onRoomMessageReceived(Message message) {
        if (message.getBody() != null) {
//            String fromName = StringUtils.parseBareAddress(message.getFrom());
            String[] strings = message.getFrom().split("/");
            String roomId = strings[0];
            String senderId = strings[1]+"@"+SERVICE;

            if ( strings[1].equals(USERNAME) )
            {
                DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
                databaseHandler.setRoomMessageStatus(message.getPacketID(), Constants.MESSAGE_STATUS_DELIVERED);
                for ( OnRoomMessageNotificationReceivedListener l : onRoomMessageNotificationReceivedListeners){
                    l.messageNotificationReceived(strings[0],message.getPacketID(),Constants.MESSAGE_STATUS_DELIVERED);
                }
                // change status to delivered
                return;
            }

            DelayInformation inf = null;
            Date time;
            try {
                inf = (DelayInformation) message.getExtension("x", "jabber:x:delay");
            } catch (Exception e) {
                Log.i("libChatConnection", " Something went wrong in setting delay");
            }

            if (inf != null) {
                time = inf.getStamp();
            } else {
                time = new Date(); // current time
            }

            ChatMessage chatMessage = new ChatMessage(senderId, roomId, time, message.getBody(), "received", message.getPacketID());

            DatabaseHandler.getInstance().saveRoomMessageToDatabase(chatMessage);

            for (OnRoomMessageReceivedListener listener : onRoomMessageReceivedListeners) {
                //Log.i("XMPPChatDemoActivity ", " Text Recieved " + message.getBody() + " from " + fromName);
                listener.roomMessageReceived(chatMessage);
            }
        }
    }

    private void onMessageReceived(Message message) {
        if (message.getBody() != null) {
            String fromName = StringUtils.parseBareAddress(message.getFrom());
//            Log.i("XMPPChatDemoActivity ", " Text Recieved " + message.getBody() + " from " + fromName);
//            messages.add(fromName + ":");
//            messages.add(message.getBody());

            DelayInformation inf = null;
            Date time;
            try {
                inf = (DelayInformation) message.getExtension("x", "jabber:x:delay");
            } catch (Exception e) {
                Log.i("libChatConnection", " Something went wrong in setting delay");
            }

            if (inf != null) {
                time = inf.getStamp();
            } else {
                time = new Date(); // current time
            }


            ChatMessage chatMessage = new ChatMessage(fromName, USERNAME, time, message.getBody(), Constants.MESSAGE_STATUS_UNREAD, message.getPacketID());
            messageEventManager.sendDeliveredNotification(fromName,message.getPacketID());
            Log.i("XMPPChatDemoActivity ", " Text Recieved " + message.getBody() + " from " + fromName);
            DatabaseHandler.getInstance().saveMessageToDatabase(chatMessage);

            for (OnMessageReceivedListener listener : onMessageReceivedListeners) {
//                Log.i("XMPPChatDemoActivity ", " Text Recieved " + message.getBody() + " from " + fromName);
                listener.messageReceived(chatMessage);
            }
        }
    }

    public void multiUserChat() {
        MultiUserChat muc = new MultiUserChat(connection, "room@conference.robotoos");

        /// create room
//        try {
//            muc.create("testbot");
//
//        } catch (XMPPException e) {
//            e.printStackTrace();
//        }


        /// join room
        try {
//            DiscussionHistory discussionHistory = new DiscussionHistory();
//            discussionHistory.setMaxStanzas(5);
//            muc.join("testbot", null, discussionHistory, 10);

            muc.join(USERNAME);

        } catch (XMPPException e1) {
            e1.printStackTrace();
        }

        try {
            muc.sendConfigurationForm(new Form(Form.TYPE_SUBMIT));
        } catch (XMPPException e) {
            e.printStackTrace();
        }

        muc.addInvitationRejectionListener(new InvitationRejectionListener() {
            public void invitationDeclined(String invitee, String reason) {
                Log.d("libchat","this person:"+invitee+" rejected invtied because:"+reason);
            }
        });

        /// invite
        muc.invite("admin@robotoos", "bia bazi");

        //  listens for MUC invitations
        MultiUserChat.addInvitationListener(connection, new InvitationListener() {
            @Override
            public void invitationReceived(org.jivesoftware.smack.Connection connection,
                                           String room, String inviter, String reason, String password, Message message) {

                MultiUserChat.decline(connection,room,inviter,"Im so busy");
            }

        });

        // get joined rooms
        //Iterator joinedRooms = MultiUserChat.getJoinedRooms(connection,"admin@robotoos/Spark 2.6.3");

        // subject change listener
        muc.addSubjectUpdatedListener(new SubjectUpdatedListener() {
            @Override
            public void subjectUpdated(String subject, String from) {
                Log.d("libchat","subject changed to "+subject+" by this "+from);
            }
        });

        try {
            muc.changeSubject("New Subject");
        } catch (XMPPException e) {
            e.printStackTrace();
        }

        muc.addMessageListener(new PacketListener() {
            @Override
            public void processPacket(Packet packet) {
                Message message = (Message) packet;
                onRoomMessageReceived(message);
                Log.d("libchat","multi chat msg:"+message.getBody());
            }
        });


    }



    public ArrayList<Room> getRoomList() {
//        return roomsMap.
        ArrayList<Room> rooms = (ArrayList<Room>) DatabaseHandler.getInstance().getAllRooms();
//        for (Map.Entry<String,MultiUserChat> entry:roomsMap.entrySet()) {
//            rooms.add(new Room(entry.getKey()));
//        }
        return rooms;
    }

    public boolean createRoom(String roomName) {
        if ( connection.isAuthenticated() == false)
            return false;

        roomName = roomName + "@conference.robotoos";

        MultiUserChat muc = new MultiUserChat(connection, roomName );

        /// create room
        try {
            muc.create(USERNAME);

            muc.sendConfigurationForm(new Form(Form.TYPE_SUBMIT));

            DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
            databaseHandler.createRoom(roomName);

            roomsMap.put(roomName,muc);
            joinRoom(muc);

            Log.d("libchat","created room: "+roomName);

            return true;
        } catch (XMPPException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean invite(String invitee, String roomName) {
        if ( connection.isAuthenticated() == false )
            return false;
        MultiUserChat multiUserChat = roomsMap.get(roomName);
        multiUserChat.invite(invitee,"Join Us");
        Log.d("libchat"," invited "+invitee+" to room "+roomName);
        return true;
    }


    public void markRoomMessageAsRead(ChatMessage message) {
        DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
        databaseHandler.setRoomMessageStatus(message.getPacketId(), Constants.MESSAGE_STATUS_READ);
        message.setStatus(Constants.MESSAGE_STATUS_READ);
//
//        if ( connection.isAuthenticated() )
//            messageEventManager.sendDisplayedNotification(message.getSenderUserId(),Constants.MESSAGE_STATUS_DISPLAYED);
//        else
//            addCommandToQueue(new NotificationCommand(message,Constants.MESSAGE_STATUS_DISPLAYED));
    }

    public void markMessageAsRead(ChatMessage message) {
        DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
        databaseHandler.setMessageStatus(message.getPacketId(),Constants.MESSAGE_STATUS_READ);
        message.setStatus(Constants.MESSAGE_STATUS_READ);

        if ( connection.isAuthenticated() )
            messageEventManager.sendDisplayedNotification(message.getSenderUserId(),Constants.MESSAGE_STATUS_DISPLAYED);
        else
            addCommandToQueue(new NotificationCommand(message,Constants.MESSAGE_STATUS_DISPLAYED));
    }

    private void addCommandToQueue(Command command){
        try {
            DatabaseHandler.getInstance().createCommand(command.toJson().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("type",type);
//            jsonObject.put("to",to);
//            jsonObject.put("body",body);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        commandQueue.put(jsonObject);
    }

    private void runCommandQueue(){
        initCommandQueue();
        DatabaseHandler.getInstance().clearCommands();
        for ( int index=0; index<commandQueue.length();++index){
            try {
                JSONObject command = (JSONObject) commandQueue.get(index);
                String type = command.getString(Command.COMMAND_TYPE);
                if ( type.equals(Command.COMMAND_TYPE_NOTIFICATION) ){
                    NotificationCommand notificationCommand = new NotificationCommand(command);
                    messageEventManager.sendDisplayedNotification(notificationCommand.getReceiver(),
                            notificationCommand.getNotification());
                }
                else if ( type.equals(Command.COMMAND_TYPE_PRIVATE_MESSAGE) ){
                    PrivateChatCommand privateChatCommand = new PrivateChatCommand(command);
                    Message message = new Message(privateChatCommand.getReceiver(), Message.Type.chat);
                    message.setBody(privateChatCommand.getBody());
                    connection.sendPacket(message);
                    DatabaseHandler.getInstance().setMessageStatus(privateChatCommand.getPacketId(), Constants.MESSAGE_STATUS_SENT);
                }
                else if ( type.equals(Command.COMMAND_TYPE_ROOM_MESSAGE) ){
                    RoomChatCommand roomChatCommand = new RoomChatCommand(command);
                    Message message = new Message(roomChatCommand.getRoom(), Message.Type.groupchat);
                    message.setBody(roomChatCommand.getBody());
                    try {
                        retrySendingRoomMessage(message, roomChatCommand.getRoom());
                        DatabaseHandler.getInstance().setRoomMessageStatus(roomChatCommand.getPacketId(), Constants.MESSAGE_STATUS_SENT);
                    } catch (XMPPException e) {
                        addCommandToQueue(roomChatCommand);
                        e.printStackTrace();
                    }

                }
                else{
                    Log.e("libchat","run command queue unknown command type");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }



    private class LibChatMessageEventNotificationListener implements MessageEventNotificationListener {

        DatabaseHandler databaseHandler;

        LibChatMessageEventNotificationListener(){
            databaseHandler = DatabaseHandler.getInstance();
        }

        boolean isRoom(String from){
            if ( from.contains("/") ){
                return true;
            }
            return false;
        }

        @Override
        public void deliveredNotification(String from, String packetID) {
            if ( isRoom(from) ){
                String[] strings = from.split("/");
                String room = strings[0];
                String sender = strings[1];
                databaseHandler.setRoomMessageStatus(packetID, Constants.MESSAGE_STATUS_DELIVERED);
                for (OnRoomMessageNotificationReceivedListener l : onRoomMessageNotificationReceivedListeners) {
                    l.messageNotificationReceived(room, packetID, Constants.MESSAGE_STATUS_DELIVERED);
                }
            }
            else{
                databaseHandler.setMessageStatus(packetID, Constants.MESSAGE_STATUS_DELIVERED);
                for (OnMessageNotificationReceivedListener l : onMessageNotificationReceivedListeners) {
                    l.messageNotificationReceivedListener(from, packetID, Constants.MESSAGE_STATUS_DELIVERED);
                }
            }

            System.out.println("The message has been delivered (" + from + ", " + packetID + ")");
        }

        public void displayedNotification(String from, String packetID) {
            if ( isRoom(from))
                Log.e("libchat","displayed notif for room");
            databaseHandler.setMessageStatus(packetID, Constants.MESSAGE_STATUS_DISPLAYED);
            for (OnMessageNotificationReceivedListener l : onMessageNotificationReceivedListeners) {
                l.messageNotificationReceivedListener(from, packetID, Constants.MESSAGE_STATUS_DISPLAYED);
            }
            System.out.println("The message has been displayed (" + from + ", " + packetID + ")");
        }

        public void composingNotification(String from, String packetID) {
            if ( isRoom(from))
                Log.e("libchat","composing notif for room");
            System.out.println("The message's receiver is composing a reply (" + from + ", " + packetID + ")");
        }

        public void offlineNotification(String from, String packetID) {
            if ( isRoom(from))
                Log.e("libchat","offline notif for room");
            System.out.println("The message's receiver is offline (" + from + ", " + packetID + ")");
        }

        public void cancelledNotification(String from, String packetID) {
            if ( isRoom(from))
                Log.e("libchat","canceled notif for room");
            System.out.println("The message's receiver cancelled composing a reply (" + from + ", " + packetID + ")");
        }

    }


}

