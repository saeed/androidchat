package com.example.chat.libChat;

/**
 * Created by araste on 01/26/2015.
 */
public interface OnMessageNotificationReceivedListener {

    void messageNotificationReceivedListener(String from, String packetId, String status);

}
