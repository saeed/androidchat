package com.example.chat.libChat;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smackx.packet.VCard;

/**
 * Created by araste on 01/07/2015.
 */
public class Contact {

    private String userid;
    private String status;
    private VCard vCard;
    private String lastActivity;

    Contact ( String userid, String status, VCard vCard, String lastActivity){
        this.userid = userid;
        this.status = status;
        this.vCard = vCard;
        this.lastActivity = lastActivity;
    }

    public String getUserName(){
        return userid;
    }

    public String getStatus(){
        return status;
    }

    public VCard getvCard() {
        return vCard;
    }

    public String getLastActivity () {return lastActivity;}
}
