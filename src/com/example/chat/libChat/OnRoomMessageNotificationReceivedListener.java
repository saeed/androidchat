package com.example.chat.libChat;

/**
 * Created by araste on 01/26/2015.
 */
public interface OnRoomMessageNotificationReceivedListener {

    void messageNotificationReceived(String from, String packetId, String status);

}
