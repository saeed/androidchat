package com.example.chat.libChat.database;

import com.orm.SugarRecord;

/**
 * Created by araste on 01/15/2015.
 */

public class LibChatDatabaseRoom extends SugarRecord<LibChatDatabaseRoom> {

    String roomid;

    public LibChatDatabaseRoom(){}

    public LibChatDatabaseRoom(String roomId){
        this.roomid = roomId;
    }

    public String getRoomId() {
        return roomid;
    }
}
