package com.example.chat.libChat.database;

import com.orm.SugarRecord;

/**
 * Created by araste on 01/15/2015.
 */

public class LibChatDatabaseCommand extends SugarRecord<LibChatDatabaseCommand> {

    String json;
    public LibChatDatabaseCommand(){}

    public LibChatDatabaseCommand(String json){
        this.json = json;
    }

    public String getJson() {
        return json;
    }
}
