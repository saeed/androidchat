package com.example.chat.libChat.database;

import com.orm.SugarRecord;

import java.util.Date;


/**
 * Created by araste on 01/15/2015.
 */

public class LibChatDatabaseRoomMessage extends SugarRecord<LibChatDatabaseRoomMessage> {
    LibChatDatabaseContact sender;
    LibChatDatabaseRoom room;
    String body;
    String time;
    String status;
    String packetid;

    public LibChatDatabaseRoomMessage(){}

    public LibChatDatabaseRoomMessage(LibChatDatabaseContact sender,
                                      LibChatDatabaseRoom room,
                                      String body,
                                      Date time,
                                      String status,
                                      String packetId){
        this.sender=sender;
        this.room = room;
        this.body=body;
        this.time=String.valueOf(time.getTime());
        this.status=status;
        this.packetid = packetId;
    }

    public LibChatDatabaseContact getSender() {
        return sender;
    }

    public LibChatDatabaseRoom getRoom() {
        return room;
    }

    public String getBody() {
        return body;
    }

    public Date getTime() {
        Date d = new Date();
        d.setTime(Long.valueOf(time).longValue());
        return d;
    }

    public String getStatus() {
        return status;
    }

    public String getPacketId() {
        return packetid;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
