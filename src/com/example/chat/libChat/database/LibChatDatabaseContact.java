package com.example.chat.libChat.database;

import com.orm.SugarRecord;

/**
 * Created by araste on 01/15/2015.
 */

public class LibChatDatabaseContact extends SugarRecord<LibChatDatabaseContact> {

    String user;

    public LibChatDatabaseContact(){}

    public LibChatDatabaseContact(String id){
        this.user = id;
    }

    public String getUserId() {
        return user;
    }
}
