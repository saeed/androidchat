package com.example.chat.libChat;

/**
 * Created by araste on 02/04/2015.
 */
public class Constants {

    public static final String MESSAGE_STATUS_DELIVERED = "delivered";
    public static final String MESSAGE_STATUS_DISPLAYED = "displayed";
    public static final String MESSAGE_STATUS_UNREAD = "unread";
    public static final String MESSAGE_STATUS_SENT = "sent";
    public static final String MESSAGE_STATUS_PENDING = "pending";
    public static final String MESSAGE_STATUS_READ = "read";



}
