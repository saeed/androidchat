package com.example.chat.libChat;

import android.util.Log;
import com.example.chat.libChat.database.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by araste on 01/15/2015.
 */
public class DatabaseHandler {
    private static DatabaseHandler databaseHandlerInstance;

    //  private Context context;

    private DatabaseHandler(/*Context context*/){

//        this.context = context;

//        Connection.getInstance().addOnMessageReceivedListener(new OnMessageReceivedListener() {
//            @Override
//            public void messageReceived(ChatMessage msg) {
//                saveMessageToDatabase(msg);
//            }
//        });
    }

    public static DatabaseHandler getInstance(){
        if ( databaseHandlerInstance == null ){
            databaseHandlerInstance  = new DatabaseHandler();
        }
        return databaseHandlerInstance ;
    }

    private LibChatDatabaseContact getOrCreateUser ( String userId ){
        List<LibChatDatabaseContact> list = LibChatDatabaseContact.find(LibChatDatabaseContact.class, "user = ?", userId);
        if ( list.size() == 0 ){
            LibChatDatabaseContact u = new LibChatDatabaseContact(userId);
            u.save();
            return u;
        }
        else if ( list.size() == 1 ){
            return list.get(0);
        }
        else
        {
            Log.e("DatabaseHandler","too many user with user id same");
            return list.get(0);
        }
    }

    private LibChatDatabaseRoom getOrCreateRoom(String roomId) {
        List<LibChatDatabaseRoom> list = LibChatDatabaseRoom.find(LibChatDatabaseRoom.class, "roomid = ?", roomId);
        if ( list.size() == 0 ){
            LibChatDatabaseRoom u = new LibChatDatabaseRoom(roomId);
            u.save();
            return u;
        }
        else if ( list.size() == 1 ){
            return list.get(0);
        }
        else
        {
            Log.e("DatabaseHandler","too many rooms with same room id:"+roomId);
            return list.get(0);
        }
    }

    public void saveMessageToDatabase(ChatMessage chatMessage) {
        LibChatDatabaseContact uFrom = getOrCreateUser(chatMessage.senderUserId);
        LibChatDatabaseContact uTo = getOrCreateUser(chatMessage.receiverUserId);
        LibChatDatabaseMessage libChatDatabaseMessage = new LibChatDatabaseMessage(uFrom,
                uTo,
                chatMessage.getBody(),
                chatMessage.getTime(),
                chatMessage.getStatus(),
                chatMessage.getPacketId());
        libChatDatabaseMessage.save();
    }

    public ArrayList<LibChatDatabaseMessage> getLastMessagesWithUser(String userId){
        return getLastMessagesWithUser(userId,5);
    }

    public ArrayList<LibChatDatabaseMessage> getLastMessagesWithUser(String userId,int limit){

        List<LibChatDatabaseContact> libChatDatabaseContacts = LibChatDatabaseContact.find(LibChatDatabaseContact.class, "user=?", userId);
        if ( libChatDatabaseContacts.size() == 0 ){
            return new ArrayList<LibChatDatabaseMessage>();
        }
        String[] strings = {libChatDatabaseContacts.get(0).getId().toString(), libChatDatabaseContacts.get(0).getId().toString()};
        List<LibChatDatabaseMessage> l = LibChatDatabaseMessage.find(LibChatDatabaseMessage.class, "(sender = ? or receiver = ?)",
                strings, null, "time DESC", String.valueOf(limit));

        return new ArrayList<LibChatDatabaseMessage> (l);
    }

    public ArrayList<LibChatDatabaseMessage> getLastConversations(){
        List<LibChatDatabaseMessage> libChatDatabaseMessageList = LibChatDatabaseMessage.findWithQuery(LibChatDatabaseMessage.class, "select sender,receiver from LIB_CHAT_DATABASE_MESSAGE group by sender,receiver;");
        HashMap< Pair<Integer,Integer> , Integer > hashMap = new HashMap<Pair<Integer, Integer>, Integer>();
        ArrayList<LibChatDatabaseMessage> results = new ArrayList<LibChatDatabaseMessage>();
        for ( LibChatDatabaseMessage m : libChatDatabaseMessageList){
            int iSender = m.getSender().getId().intValue();
            int iReceiver = m.getReceiver().getId().intValue();

            Pair<Integer,Integer> key1 = new Pair<Integer, Integer>(iSender,iReceiver);
            Pair<Integer,Integer> key2 = new Pair<Integer, Integer>(iReceiver,iSender);

            if ( hashMap.containsKey(key1) || hashMap.containsKey(key2) ){
                continue;
            }

            String senderId = m.getSender().getId().toString();
            String receiverId = m.getReceiver().getId().toString();

            String [] strings = { senderId,receiverId,senderId,receiverId};

            List<LibChatDatabaseMessage> l = LibChatDatabaseMessage.find(LibChatDatabaseMessage.class, "(sender = ? and receiver = ?) or (receiver = ? and sender = ?)",
                    strings, null, "time DESC", "1");
//            String q = String.format("select max(id) from message m\n" +
//                    "where (m.sender = %d and m.receiver = %d) or (m.receiver = %d and m.sender = %d);",
//                    senderId,receiverId,senderId,receiverId);
//            List<Message> l = Message.findWithQuery(Message.class, q);
//            Message.
            LibChatDatabaseMessage msg = LibChatDatabaseMessage.findById(LibChatDatabaseMessage.class, l.get(0).getId());
            results.add(msg);
            hashMap.put(key1,l.get(0).getId().intValue());
            hashMap.put(key2,l.get(0).getId().intValue());
        }
        return results;
    }

    public void setRoomMessageStatus(String packetId, String staus) {
        List<LibChatDatabaseRoomMessage> messages = LibChatDatabaseRoomMessage.find(LibChatDatabaseRoomMessage.class,
                "packetId = ?",packetId);
        if ( messages.size() != 1 ){
            if ( messages.size() == 0 )
                Log.e("libchat","no messages with this packet");
            else
                Log.e("libchat","too many messages with one packet id");
        }else{
            LibChatDatabaseRoomMessage message = messages.get(0);
            message.setStatus(staus);
            message.save();
        }
    }

    void setMessageStatus(String packetId, String status){
        List<LibChatDatabaseMessage> msgList = LibChatDatabaseMessage.find(LibChatDatabaseMessage.class,
                "packetId = ?", packetId);
        if ( msgList.size() != 1 ){
            if ( msgList.size() == 0 )
                Log.e("libchat","no messages with this packet");
            else
                Log.e("libchat","too many messages with one packet id");
        }else{
            LibChatDatabaseMessage message = msgList.get(0);
            message.setStatus(status);
            message.save();
        }
    }

    public void saveRoomMessageToDatabase(ChatMessage chatMessage) {
        LibChatDatabaseContact uFrom = getOrCreateUser(chatMessage.senderUserId);
        LibChatDatabaseRoom room = getOrCreateRoom(chatMessage.receiverUserId);
        LibChatDatabaseRoomMessage libChatDatabaseRoomMessage = new LibChatDatabaseRoomMessage(uFrom,
                room,chatMessage.getBody(),
                chatMessage.getTime(),
                chatMessage.getStatus(),
                chatMessage.getPacketId());
        libChatDatabaseRoomMessage.save();
    }

    public ArrayList<LibChatDatabaseRoomMessage> getLastRoomMessages(String room) {

        List<LibChatDatabaseRoom> libChatDatabaseRooms = LibChatDatabaseRoom.find(LibChatDatabaseRoom.class, "roomid=?", room);
        if ( libChatDatabaseRooms.size() == 0 ){
            return new ArrayList<LibChatDatabaseRoomMessage>();
        }
        String room_id = libChatDatabaseRooms.get(0).getId().toString();
        String[] strings = {room_id};
        List<LibChatDatabaseRoomMessage> l = LibChatDatabaseRoomMessage.find(LibChatDatabaseRoomMessage.class, "(room = ?)",
                strings, null, "time DESC", String.valueOf(5));

        return new ArrayList<LibChatDatabaseRoomMessage> (l);

    }

    public List<Room> getAllRooms() {
        List<LibChatDatabaseRoom> libChatDatabaseRooms = LibChatDatabaseRoom.find(LibChatDatabaseRoom.class, "1=1");
        List<Room> rooms = new ArrayList<Room>();
        for ( LibChatDatabaseRoom room : libChatDatabaseRooms){
            rooms.add(new Room(room.getRoomId()));
        }
        return rooms;
    }

    public void createRoom(String roomName) {
        LibChatDatabaseRoom room = new LibChatDatabaseRoom(roomName);
        room.save();
    }

    public List<Contact> getAllContacts() {
        List<LibChatDatabaseContact> libChatDatabaseContacts = LibChatDatabaseContact.find(LibChatDatabaseContact.class,"1=1");
        List <Contact> contacts = new ArrayList<Contact>();
        for ( LibChatDatabaseContact contact : libChatDatabaseContacts ){
            contacts.add(new Contact(contact.getUserId(),null,null,null));
        }
        return contacts;
    }

    public JSONArray getAllCommands() throws JSONException {
        List<LibChatDatabaseCommand> libChatDatabaseCommands = LibChatDatabaseCommand.find(LibChatDatabaseCommand.class,"1=1");
        JSONArray jsonArray = new JSONArray();
        for ( LibChatDatabaseCommand command : libChatDatabaseCommands ){
            JSONObject object = new JSONObject( command.getJson() );
            jsonArray.put(object);
        }
        return jsonArray;
    }

    public void createCommand(String json) {
        LibChatDatabaseCommand command = new LibChatDatabaseCommand(json);
        command.save();
    }

    public void clearCommands()
    {
        LibChatDatabaseCommand.deleteAll(LibChatDatabaseCommand.class);
    }


}
