package com.example.chat.libChat.Command;

import com.example.chat.libChat.ChatMessage;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by araste on 02/04/2015.
 */
public class RoomChatCommand implements Command {


    private static final String ROOM = "room";
    private static final String PACKET_ID = "packet_id";
    private static final String BODY = "body";

    private String room;
    private String packetId;
    private String body;

    public RoomChatCommand(ChatMessage chatMessage){
        room = chatMessage.getReceiverUserId();
        packetId = chatMessage.getPacketId();
        body = chatMessage.getBody();
    }

    public RoomChatCommand(JSONObject jsonObject) throws JSONException {
        room = jsonObject.getString(ROOM);
        packetId = jsonObject.getString(PACKET_ID);
        body = jsonObject.getString(BODY);
    }


    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(COMMAND_TYPE,COMMAND_TYPE_ROOM_MESSAGE);
        jsonObject.put(ROOM,room);
        jsonObject.put(PACKET_ID,packetId);
        jsonObject.put(BODY,body);
        return jsonObject;
    }

    public String getRoom() {
        return room;
    }

    public String getPacketId() {
        return packetId;
    }

    public String getBody() {
        return body;
    }
}
