package com.example.chat.libChat.Command;


import com.example.chat.libChat.ChatMessage;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by araste on 02/04/2015.
 */
public class NotificationCommand implements Command {

    private static final String RECEIVER = "receiver";
    private static final String NOTIFICATION = "notification";

    private String receiver;
    private String notification;

    public NotificationCommand(ChatMessage chatMessage, String notification){
        receiver = chatMessage.getReceiverUserId();
        this.notification = notification;
    }

    public NotificationCommand(JSONObject jsonObject) throws JSONException {
        receiver = jsonObject.getString(RECEIVER);
        notification = jsonObject.getString(NOTIFICATION);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put(COMMAND_TYPE,COMMAND_TYPE_NOTIFICATION);
        jsonObject.put(RECEIVER,receiver);
        jsonObject.put(NOTIFICATION,notification);
        return null;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getNotification() {
        return notification;
    }
}
