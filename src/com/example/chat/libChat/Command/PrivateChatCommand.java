package com.example.chat.libChat.Command;

import com.example.chat.libChat.ChatMessage;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by araste on 02/04/2015.
 */
public class PrivateChatCommand implements Command {

    private static final String RECEIVER = "receiver";
    private static final String PACKET_ID = "packet_id";
    private static final String BODY = "body";

    private String receiver;
    private String body;
    private String packetId;

    public PrivateChatCommand(ChatMessage chatMessage){
        receiver = chatMessage.getReceiverUserId();
        body = chatMessage.getBody();
        packetId = chatMessage.getPacketId();
    }

    public PrivateChatCommand(JSONObject jsonObject) throws JSONException {
        receiver = jsonObject.getString(RECEIVER);
        body = jsonObject.getString(BODY);
        packetId = jsonObject.getString(PACKET_ID);
    }

    public String getReceiver() {
        return receiver;
    }

    public String getBody() {
        return body;
    }

    public String getPacketId() {
        return packetId;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(COMMAND_TYPE,COMMAND_TYPE_PRIVATE_MESSAGE);
        jsonObject.put(RECEIVER,receiver);
        jsonObject.put(PACKET_ID,packetId);
        jsonObject.put(BODY,body);
        return jsonObject;
    }
}
