package com.example.chat.libChat.Command;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by araste on 02/04/2015.
 */
public interface Command {

    static final String COMMAND_TYPE_NOTIFICATION = "notification" ;
    static final String COMMAND_TYPE_PRIVATE_MESSAGE = "private_message" ;
    static final String COMMAND_TYPE_ROOM_MESSAGE = "room_message";

    static final String COMMAND_TYPE = "type";


    JSONObject toJson() throws JSONException;
}
