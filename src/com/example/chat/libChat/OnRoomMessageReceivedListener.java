package com.example.chat.libChat;

/**
 * Created by araste on 01/13/2015.
 */
public interface OnRoomMessageReceivedListener {

    void roomMessageReceived(ChatMessage msg);

}
