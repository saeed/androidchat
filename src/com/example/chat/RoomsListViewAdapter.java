package com.example.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.chat.libChat.Contact;
import com.example.chat.libChat.Room;

import java.util.ArrayList;

/**
 * Created by araste on 01/07/2015.
 */
public class RoomsListViewAdapter extends ArrayAdapter<Room> {

    Context context;
    ArrayList<Room> contactArrayList;

    public RoomsListViewAdapter(Context context, ArrayList<Room> contacts){
        super(context,R.layout.contact_list_item,contacts);

        this.context = context;
        this.contactArrayList = contacts;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.contact_list_item,parent,false);

        TextView name = (TextView) rowView.findViewById(R.id.name);
        TextView status = (TextView) rowView.findViewById(R.id.status);
        TextView lastSeen = (TextView) rowView.findViewById(R.id.last_seen_text_view);

        name.setText(contactArrayList.get(position).getRoomName());
//        status.setText(contactArrayList.get(position).getStatus());
//        lastSeen.setText(contactArrayList.get(position).getLastActivity());
        return rowView;
    }
}
