package com.example.chat;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.example.chat.libChat.*;
import com.example.chat.libChat.database.LibChatDatabaseMessage;
import com.example.chat.libChat.database.LibChatDatabaseRoomMessage;
import org.jivesoftware.smack.packet.Message;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by araste on 01/07/2015.
 */
public class RoomActivity extends Activity {

    private ArrayList<ChatMessage> chatItemArrayList;
    private ListView chatListView;
    private ChatListViewAdapter chatListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_activity);

        final Context context = this;

        chatItemArrayList = new ArrayList<ChatMessage>();
        chatListView = (ListView) findViewById(R.id.chat_list);
        chatListViewAdapter = new ChatListViewAdapter(this,chatItemArrayList);
        chatListView.setAdapter(chatListViewAdapter);

        initHistory();

        updateList();

        TextView contactTextView = (TextView) findViewById(R.id.room_name_text_view);
        final String roomName = getIntent().getStringExtra("room");
        contactTextView.setText(roomName);

        final Connection connection = Connection.getInstance();

        Button send = (Button) this.findViewById(R.id.send_button);
        final EditText editText = (EditText) this.findViewById(R.id.editTextMessage);
        send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String text = editText.getText().toString();

                ChatMessage chatMessage = new ChatMessage();
                connection.sendRoomMessage(roomName, text, chatMessage);

                chatItemArrayList.add(chatMessage);
                updateList();
            }
        });

        final EditText invite_edit_text = (EditText) this.findViewById(R.id.invite_edit_text);
        Button invite_button = (Button) this.findViewById(R.id.invite_button);
        invite_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String invitee = invite_edit_text.getText().toString();
                connection.invite(invitee,roomName);

            }
        });

        connection.addOnRoomMessageReceivedListener(new OnRoomMessageReceivedListener() {
            @Override
            public void roomMessageReceived(ChatMessage msg) {
                if ( msg.getReceiverUserId().equals(getIntent().getStringExtra("room")) == false ){
                    return ;
                }
                chatItemArrayList.add(msg);
                connection.markRoomMessageAsRead(msg);
                updateList();
            }
        });

        connection.addOnMessageNotificationReceivedListener(new OnMessageNotificationReceivedListener() {
            @Override
            public void messageNotificationReceivedListener(String from, String packetId, String status) {
//                Toast.makeText(getApplicationContext(),"notification status:"+status+" packetId: "+packetId,Toast.LENGTH_LONG).show();

                for (ChatMessage chatMessage:chatItemArrayList){
                    if ( chatMessage.getPacketId().equals( packetId )){
                        chatMessage.setStatus(status);
                        updateList();
                        break;
                    }
                }
            }
        });


    }

    private void initHistory(){
        DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
        ArrayList<LibChatDatabaseRoomMessage> list = databaseHandler.
                getLastRoomMessages(getIntent().getStringExtra("room"));

        for (LibChatDatabaseRoomMessage message :list) {
            //ChatItem item = new ChatItem(libChatDatabaseMessage.getSender().getUserId(), libChatDatabaseMessage.getBody());
            ChatMessage item = new ChatMessage(message.getSender().getUserId(),
                    message.getRoom().getRoomId(),
                    message.getTime(),
                    message.getBody(),
                    message.getStatus(),
                    message.getPacketId());
            if ( item.getStatus().equals(Constants.MESSAGE_STATUS_UNREAD) ){
                Connection.getInstance().markRoomMessageAsRead(item);
            }
            chatItemArrayList.add(item);
        }
    }


    private void updateList(){
        ChatListViewAdapter chatListViewAdapter = (ChatListViewAdapter) chatListView.getAdapter();
        chatListViewAdapter.notifyDataSetChanged();
        chatListView.postInvalidate();
//        chatListViewAdapter.notifyDataSetChanged();
//        chatListView.setAdapter(chatListViewAdapter);

    }
}
