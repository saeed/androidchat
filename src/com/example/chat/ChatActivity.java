package com.example.chat;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.example.chat.libChat.*;
import com.example.chat.libChat.Connection;
import com.example.chat.libChat.database.LibChatDatabaseMessage;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.packet.Message;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by araste on 01/07/2015.
 */
public class ChatActivity extends Activity {

    private ArrayList<ChatMessage> chatItemArrayList;
    private ListView chatListView;
    private ChatListViewAdapter chatListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);

        final Context context = this;

        chatItemArrayList = new ArrayList<ChatMessage>();
        chatListView = (ListView) findViewById(R.id.chat_list);
        chatListViewAdapter = new ChatListViewAdapter(this,chatItemArrayList);
        chatListView.setAdapter(chatListViewAdapter);

        initHistory();

        updateList();

        TextView contactTextView = (TextView) findViewById(R.id.contactTextView);
        final String contact = getIntent().getStringExtra("contact");
        contactTextView.setText(contact);

        final Connection connection = Connection.getInstance();

        Button send = (Button) this.findViewById(R.id.send_button);
        final EditText editText = (EditText) this.findViewById(R.id.editTextMessage);
        send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String to = contact;
                String body = editText.getText().toString();
                ChatMessage chatMessage = new ChatMessage() ;
                Log.i("XMPPChatDemoActivity ", "Sending text " + body + " to " + to);
                connection.sendMessage(to,body,chatMessage);

                //DatabaseHandler.getInstance().saveMessageToDatabase(chatMessage);

                chatItemArrayList.add(chatMessage);
                updateList();
//                if (connection != null) {
//                    connection.sendPacket(msg);
//                    messages.add(connection.getUser() + ":");
//                    messages.add(text);
//                    setListAdapter();
//                }
            }
        });


        connection.addOnMessageReceivedListener(new OnMessageReceivedListener() {
            @Override
            public void messageReceived(ChatMessage message) {
                if ( message.getSenderUserId().equals(contact) == false )
                    return;
                chatItemArrayList.add(message);
                connection.markMessageAsRead(message);
                updateList();
                //                chatListView.postInvalidate();
            }
        });

        connection.addOnMessageNotificationReceivedListener(new OnMessageNotificationReceivedListener() {
            @Override
            public void messageNotificationReceivedListener(String from, String packetId, String status) {
//                Toast.makeText(getApplicationContext(),"notification status:"+status+" packetId: "+packetId,Toast.LENGTH_LONG).show();

                for (ChatMessage chatMessage:chatItemArrayList){
                    if ( chatMessage.getPacketId().equals( packetId )){
                        chatMessage.setStatus(status);
                        updateList();
                        break;
                    }
                }
            }
        });


    }

    private void initHistory(){
        DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
        ArrayList<LibChatDatabaseMessage> list = databaseHandler.
                getLastMessagesWithUser(getIntent().getStringExtra("contact"));

        for (LibChatDatabaseMessage libChatDatabaseMessage :list) {
            //ChatItem item = new ChatItem(libChatDatabaseMessage.getSender().getUserId(), libChatDatabaseMessage.getBody());
            ChatMessage item = new ChatMessage(libChatDatabaseMessage.getSender().getUserId(),
                    libChatDatabaseMessage.getReceiver().getUserId(),
                    libChatDatabaseMessage.getTime(),
                    libChatDatabaseMessage.getBody(),
                    libChatDatabaseMessage.getStatus(),
                    libChatDatabaseMessage.getPacketId());
            if ( item.getStatus().equals(Constants.MESSAGE_STATUS_UNREAD))
                Connection.getInstance().markMessageAsRead(item);
            chatItemArrayList.add(item);
        }
    }


    private void updateList(){
        ChatListViewAdapter chatListViewAdapter = (ChatListViewAdapter) chatListView.getAdapter();
        chatListViewAdapter.notifyDataSetChanged();
        chatListView.postInvalidate();
//        chatListViewAdapter.notifyDataSetChanged();
//        chatListView.setAdapter(chatListViewAdapter);

    }
}
