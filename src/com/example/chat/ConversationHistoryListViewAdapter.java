package com.example.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.chat.libChat.database.LibChatDatabaseMessage;

import java.util.ArrayList;

/**
 * Created by araste on 01/07/2015.
 */
public class ConversationHistoryListViewAdapter extends ArrayAdapter<LibChatDatabaseMessage> {

    Context context;
    ArrayList<LibChatDatabaseMessage> historyArrayList;

    public ConversationHistoryListViewAdapter(Context context, ArrayList<LibChatDatabaseMessage> historyArrayList){
        super(context,R.layout.contact_list_item,historyArrayList);

        this.context = context;
        this.historyArrayList = historyArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.conversation_history_item,parent,false);

        TextView sender = (TextView) rowView.findViewById(R.id.text_view_sender);
        TextView receiver = (TextView) rowView.findViewById(R.id.text_view_receiver);
        TextView body = (TextView) rowView.findViewById(R.id.text_view_body);

        receiver.setText(historyArrayList.get(position).getReceiver().getUserId());
        sender.setText(historyArrayList.get(position).getSender().getUserId());
        body.setText(historyArrayList.get(position).getBody().toString());
        return rowView;
    }
}
