package com.example.chat;


import java.util.ArrayList;

import android.content.Intent;
import com.example.chat.libChat.Connection;
import com.example.chat.libChat.DatabaseHandler;
import com.example.chat.libChat.database.LibChatDatabaseMessage;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import org.jivesoftware.smack.SmackAndroid;

public class MyActivity extends Activity {

    private Connection connection;
//    private EditText recipient;
//    private EditText textMessage;
    private ListView historyListView;
    private ArrayList<LibChatDatabaseMessage> historyItems ;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        SmackAndroid.init(getApplicationContext());
//        recipient = (EditText) this.findViewById(R.id.toET);
        final EditText create_room_edit_text = (EditText) this.findViewById(R.id.create_room_edit_text);
        Button create_room_button = (Button) this.findViewById(R.id.create_room_button);

        historyItems = new ArrayList<LibChatDatabaseMessage>();
        //chatListView = (ListView) findViewById(R.id.chat_list);
        //chatListViewAdapter = new ChatListViewAdapter(this,chatItemArrayList);
        //chatListView.setAdapter(chatListViewAdapter);

        historyListView = (ListView) this.findViewById(R.id.listMessages);

        connection = Connection.getInstance();
        DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
        databaseHandler.getLastConversations();


        setListAdapter();

        //
        Button button = (Button) findViewById(R.id.goToContactsActivtyButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyActivity.this,ContactsActivity.class);
                startActivity(intent);
            }
        });

        Button addContactButton = (Button) findViewById(R.id.addContactButton);
        addContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connection.addContact("sudomakeinstall2@robotoos");
            }
        });


        Button multiUserChatButton = (Button) findViewById(R.id.addMultiuserChat);
        multiUserChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyActivity.this,RoomsActivity.class);
                startActivity(intent);
            }
        });

     //   connection.connect();

        create_room_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String new_room_name = create_room_edit_text.getText().toString();
                connection.createRoom(new_room_name);
            }
        });

    }

    /**
     * Called by Settings dialog when a connection is establised with
     * the XMPP server
     */

    private void setListAdapter() {
        DatabaseHandler databaseHandler = DatabaseHandler.getInstance();
        historyItems = databaseHandler.getLastConversations();
        ConversationHistoryListViewAdapter adapter = new ConversationHistoryListViewAdapter(this,historyItems);
        historyListView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            connection.disconnect();
        } catch (Exception e) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setListAdapter();
    }
}