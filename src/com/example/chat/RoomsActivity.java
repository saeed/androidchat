package com.example.chat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.example.chat.libChat.Connection;
import com.example.chat.libChat.Contact;
import com.example.chat.libChat.Room;

import java.util.ArrayList;

/**
 * Created by araste on 01/07/2015.
 */
public class RoomsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_activity);

        ListView contactsListView = (ListView) findViewById(R.id.contacts_list);
        final ArrayList<Room> roomsArrayList = Connection.getInstance().getRoomList();
        RoomsListViewAdapter contactListViewAdapter = new RoomsListViewAdapter(this,roomsArrayList);
        contactsListView.setAdapter(contactListViewAdapter);

        contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(RoomsActivity.this,RoomActivity.class);
                intent.putExtra("room",roomsArrayList.get(i).getRoomName());
                startActivity(intent);
            }
        });




    }
}
